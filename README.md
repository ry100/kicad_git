# 290 lab: kicad_git

# Erica Yan

# v1.0 to v2.0:
# Modify circuit to have the toggle switch (power on/off) off the board, connecting to the PCB with a 1x2-pin socket connector.
# Modify 1x6 pin header on the PCB to now be a 1x8-pin header to accomodate this extra connection.
# Apart from the two requirements above, I also made some other edits to the circuit from v1.0 to v2.0 because I realized that my original kicad schematic wasn't completely correct